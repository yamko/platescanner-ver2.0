package com.yamko.platescanner.controller;

import com.yamko.platescanner.logic.AlprScanner;
import com.yamko.platescanner.view.VideoPlayer;
import com.yamko.platescanner.model.LicensePlate;
import com.yamko.platescanner.utils.DialogBuilder;
import com.yamko.platescanner.utils.ImageUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    //---Image-----
    @FXML
    private Button btnSelectImage;
    @FXML
    private Button btnSelectVideo;
    @FXML
    private Button btnSelectFolder;
    @FXML
    private HBox imageResultContainer;
    @FXML
    private TextField tfPlateNumber;
    @FXML
    private TextField tfPlateConfidence;
    @FXML
    private ListView<LicensePlate> itemView;
    @FXML
    private ImageView ivImage;


    //-----Video-----
    @FXML
    private Button btSnapshotFolder;
    @FXML
    private Button btPlay;
    @FXML
    private Button btStop;
    @FXML
    private Button btSnapshot;
    @FXML
    private AnchorPane mediaContainer;
    @FXML
    private HBox videoControlLay;
    @FXML
    private Button btScanSnapshots;

    private Stage stage;
    private VideoPlayer videoPlayer;
    private File snapshotFolder;
    private ArrayList<LicensePlate> plateList;
    private ArrayList<String> imagePaths;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ButtonClickListener listener = new ButtonClickListener();
        btnSelectImage.setOnMouseClicked(listener);
        btnSelectFolder.setOnMouseClicked(listener);
        btnSelectVideo.setOnMouseClicked(listener);
        btSnapshotFolder.setOnMouseClicked(listener);
        btPlay.setOnMouseClicked(listener);
        btStop.setOnMouseClicked(listener);
        btSnapshot.setOnMouseClicked(listener);
        btScanSnapshots.setOnMouseClicked(listener);
        itemView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (!imageResultContainer.isVisible()) {
                    drawImageLay();
                }
                LicensePlate pl = itemView.getSelectionModel().getSelectedItem();
                ivImage.setImage(new Image("file:" + pl.getImagePath()));
                tfPlateNumber.setText(pl.getLicenseNumber());
                tfPlateConfidence.setText(pl.getConfidence());
            }
        });
    }

    private void drawImageLay() {
        mediaContainer.getChildren().clear();
        ivImage = new ImageView();
        ivImage.setFitHeight(500d);
        ivImage.setFitWidth(833d);
        mediaContainer.getChildren().add(0, ivImage);
        imageResultContainer.setVisible(true);
        videoControlLay.setVisible(false);
    }

    private void drawVideoLay() {
        imageResultContainer.setVisible(false);
        videoControlLay.setVisible(true);
        mediaContainer.getChildren().clear();
    }

    private void updateList() {
        itemView.getItems().clear();
        ObservableList<LicensePlate> list = FXCollections.observableList(plateList);
        itemView.setItems(list);
        itemView.refresh();
        System.gc();
    }

    private class ButtonClickListener implements EventHandler<MouseEvent> {
        @Override
        public void handle(MouseEvent event) {
            if (stage == null) {
                stage = (Stage) btnSelectImage.getScene().getWindow();
            }
            if (!(event.getSource() instanceof Button)) {
                return;
            }
            List<File> fileList;
            switch (((Button) event.getSource()).getId()) {
                case "btnSelectImage":
                    fileList = DialogBuilder.fileSelectDialog(stage, "Choose image", DialogBuilder.IMAGE);
                    if (fileList != null) {
                        ArrayList<String> pathes = new ArrayList<>();
                        for (File file : fileList) {
                            pathes.add(file.getAbsolutePath());
                        }
                        plateList = new AlprScanner().scanFiles(pathes);
                        drawImageLay();
                        updateList();
                    }
                    break;
                case "btnSelectFolder":
                    File directory = DialogBuilder.folderSelectDialog(stage, "Select folder");
                    if (directory != null) {
                        try {
                            imagePaths = ImageUtils.getAllImages(directory, false);
                            plateList = new AlprScanner().scanFiles(imagePaths);
                            drawImageLay();
                            updateList();
                            imagePaths = null;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case "btnSelectVideo":
                    if (snapshotFolder == null) {
                        DialogBuilder.alertDialog("Error", "Please select folder for snapshots");
                        return;
                    }
                    fileList = DialogBuilder.fileSelectDialog(stage, "Select file", DialogBuilder.VIDEO);
                    if (fileList == null) {
                        return;
                    }
                    if (videoPlayer == null) {
                        videoPlayer = new VideoPlayer(stage, mediaContainer);
                    }
                    imagePaths = new ArrayList<>();
                    drawVideoLay();
                    videoPlayer.playVideo(fileList.get(0));
                    break;
                case "btSnapshotFolder":
                    snapshotFolder = DialogBuilder.folderSelectDialog(stage, "Select folder for snapshots");
                    break;
                case "btPlay":
                    Image imageDecline;
                    if (!videoPlayer.isVideoPlaying()) {
                        videoPlayer.resumeVideo();
                        imageDecline = new Image(getClass().getResourceAsStream("/drawable/pause-circle.png"));
                    } else {
                        videoPlayer.pauseVideo();
                        imageDecline = new Image(getClass().getResourceAsStream("/drawable/play-circle.png"));
                    }
                    btPlay.setGraphic(new ImageView(imageDecline));
                    break;
                case "btStop":
                    videoPlayer.stopVideo();
                    break;
                case "btSnapshot":
                    WritableImage image = videoPlayer.takeSnapshot();
                    ImageUtils.writableImageToBytes(image);
                    imagePaths.add(ImageUtils.writableImageToImage(image, new Date().getTime(), snapshotFolder.getAbsolutePath()));
                    break;
                case "btScanSnapshots":
                    if (!imagePaths.isEmpty()) {
                        videoPlayer.stopVideo();
                        plateList = new AlprScanner().scanFiles(imagePaths);
                        drawImageLay();
                        updateList();
                        imagePaths = null;
                    }
                    break;
                default:
                    throw new IllegalArgumentException();
            }
        }
    }
}
