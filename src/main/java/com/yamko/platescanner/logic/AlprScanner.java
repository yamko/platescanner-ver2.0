package com.yamko.platescanner.logic;


import com.openalpr.jni.Alpr;
import com.openalpr.jni.AlprPlate;
import com.openalpr.jni.AlprPlateResult;
import com.openalpr.jni.AlprResults;
import com.yamko.platescanner.model.LicensePlate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yamko on 11/18/15.
 */
public class AlprScanner {

    private final String country = "us";
    private final String runtimeDataDir = "/home/yamko/kursov/openalpr/runtime_data";
    private final int count = 1;

    private Alpr alpr;

    public AlprScanner() {
        alpr = new Alpr(country, "", runtimeDataDir);
        alpr.setTopN(count);
    }

    public ArrayList<LicensePlate> scanFiles(ArrayList<String> source) {
        ArrayList<LicensePlate> recognized = new ArrayList<>();
        AlprResults result;
        for (String imagePath : source) {
            result = alpr.recognize(imagePath);
            recognized.add(parseResult(result.getPlates(), imagePath));
        }
        alpr.unload();
        return recognized;
    }

    private LicensePlate parseResult(List<AlprPlateResult> results, String path) {
        LicensePlate license = new LicensePlate();
        if (results.size() == 0) {
            license.setLicenseNumber("Number wasn't found");
            license.setImagePath(path);
        }
        for (AlprPlateResult result : results) {
            for (AlprPlate plate : result.getTopNPlates()) {
                license.setLicenseNumber(plate.getCharacters());
                license.setConfidence(String.format("%-5f",plate.getOverallConfidence()));
                license.setImagePath(path);
            }
        }
        return license;
    }
}
