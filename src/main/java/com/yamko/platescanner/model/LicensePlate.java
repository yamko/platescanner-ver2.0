package com.yamko.platescanner.model;

import java.nio.file.Paths;

/**
 * Created by yamko on 11/30/15.
 */
public class LicensePlate {
    private String imagePath;
    private String licenseNumber;
    private String confidence;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getConfidence() {
        return confidence;
    }

    public void setConfidence(String result) {
        this.confidence = result;
    }

    @Override
    public String toString() {
        return Paths.get(imagePath).getFileName().toString();
    }
}
