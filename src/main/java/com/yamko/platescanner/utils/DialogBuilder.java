package com.yamko.platescanner.utils;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.List;

/**
 * Created by yamko on 11/17/15.
 */
public class DialogBuilder {

    public static final String IMAGE = "image";
    public static final String VIDEO = "video";

    @FXML
    public static List<File> fileSelectDialog(Stage stage, String title, String extension) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        FileChooser.ExtensionFilter filter;
        if (extension != null) {
            if (extension.equals(IMAGE)) {
                filter = new FileChooser.ExtensionFilter("Image files", "*.JPG", "*.jpg", "*.JPEG", "*.jpeg", "*.png", "*.PNG");
            } else {
                filter = new FileChooser.ExtensionFilter("Video files", "*.MP4", "*.mp4");
            }
            fileChooser.getExtensionFilters().addAll(filter);
        }
        return fileChooser.showOpenMultipleDialog(stage);
    }

    @FXML
    public static File folderSelectDialog(Stage stage, String title) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle(title);
        return directoryChooser.showDialog(stage);
    }

    @FXML
    public static void alertDialog(String title, String text){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(text);

        alert.showAndWait();
    }
}
