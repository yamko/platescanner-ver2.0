package com.yamko.platescanner.utils;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by yamko on 11/23/15.
 */
public class ImageUtils {

    public static ArrayList<String> getAllImages(File directory, boolean descendIntoSubDirectories) throws IOException {
        ArrayList<String> resultList = new ArrayList<>();
        File[] f = directory.listFiles();
        for (File file : f) {
            if (file != null && (file.getName().toLowerCase().endsWith(".jpg") || file.getName().toLowerCase().endsWith(".png"))) {
                resultList.add(file.getCanonicalPath());
            }
            if (descendIntoSubDirectories && file.isDirectory()) {
                ArrayList<String> tmp = getAllImages(file, true);
                if (tmp != null) {
                    resultList.addAll(tmp);
                }
            }
        }
        if (!resultList.isEmpty())
            return resultList;
        else
            return null;
    }

    public static byte[] writableImageToBytes(WritableImage wim) {
        ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(wim, null), "png", byteOutput);
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return byteOutput.toByteArray();
    }

    public static String writableImageToImage(WritableImage wim, long name, String rootFolder) {
        try {
            String path = rootFolder + "/"+ name +".png";
            ImageIO.write(SwingFXUtils.fromFXImage(wim, null), "png", new File(path));
            return path;
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }
}
