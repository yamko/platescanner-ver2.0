package com.yamko.platescanner.view;
/**
 * Created by yamko on 11/30/15.
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainWindow extends Application {

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        String fxmlFile = "/fxml/main_window.fxml";
        FXMLLoader loader = new FXMLLoader();
        Parent root = (Parent) loader.load(getClass().getResource(fxmlFile));
        stage.setTitle("Plate scanner");
        stage.setScene(new Scene(root, 1074, 565));
        stage.setResizable(false);
        stage.show();
    }
}
