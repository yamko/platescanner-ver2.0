package com.yamko.platescanner.view;

/**
 * Created by yamko on 11/29/15.
 */

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Slider;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;

public class VideoPlayer {

    private static final int WIDTH = 833;
    private static final int HEIGHT = 500;

    private Stage stage;
    private AnchorPane root;
    private MediaPlayer player;
    private MediaView view;
    private boolean isVideoPlaying;

    public VideoPlayer(Stage stage, AnchorPane root) {
        this.stage = stage;
        this.root = root;
    }

    @FXML
    public void playVideo(File file ) {
        Media media = new Media(file.toURI().toString());
        player = new MediaPlayer(media);
        view = new MediaView(player);
        view.setFitWidth(WIDTH);
        view.setFitHeight(HEIGHT);

        final Timeline slideIn = new Timeline();
        final Timeline slideOut = new Timeline();

        root.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                slideOut.play();
            }
        });
        root.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                slideIn.play();
            }
        });
        final VBox vbox = new VBox();
        final Slider slider = new Slider();
        vbox.getChildren().add(slider);

        final HBox hbox = new HBox(2);
        vbox.getChildren().add(hbox);
        root.getChildren().add(view);
        root.getChildren().add(vbox);

        player.play();
        isVideoPlaying = true;
        player.setOnReady(new Runnable() {
            @Override
            public void run() {

                hbox.setMinWidth(WIDTH);

                stage.setMinWidth(WIDTH);
                stage.setMinHeight(HEIGHT);

                vbox.setMinSize(WIDTH, 100);
                vbox.setTranslateY(HEIGHT - 100);

                slider.setMin(0.0);
                slider.setValue(0.0);
                slider.setMax(player.getTotalDuration().toSeconds());

                slideOut.getKeyFrames().addAll(
                        new KeyFrame(new Duration(0),
                                new KeyValue(vbox.translateYProperty(), HEIGHT - 100),
                                new KeyValue(vbox.opacityProperty(), 0.9)
                        ),
                        new KeyFrame(new Duration(300),
                                new KeyValue(vbox.translateYProperty(), HEIGHT),
                                new KeyValue(vbox.opacityProperty(), 0.0)
                        )
                );
                slideIn.getKeyFrames().addAll(
                        new KeyFrame(new Duration(0),
                                new KeyValue(vbox.translateYProperty(), HEIGHT),
                                new KeyValue(vbox.opacityProperty(), 0.0)
                        ),
                        new KeyFrame(new Duration(300),
                                new KeyValue(vbox.translateYProperty(), HEIGHT - 100),
                                new KeyValue(vbox.opacityProperty(), 0.9)
                        )
                );
            }
        });

        player.currentTimeProperty().addListener(new ChangeListener<Duration>() {
            @Override
            public void changed(ObservableValue<? extends Duration> observableValue, Duration duration, Duration current) {
                slider.setValue(current.toSeconds());
            }
        });

        slider.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                player.seek(Duration.seconds(slider.getValue()));
            }
        });
    }

    @FXML
    public void stopVideo(){
        player.stop();
        isVideoPlaying = false;
    }

    @FXML
    public void pauseVideo(){
        isVideoPlaying = false;
        player.pause();
    }

    @FXML
    public void resumeVideo(){
        player.play();
        isVideoPlaying = true;
    }

    @FXML
    public WritableImage takeSnapshot(){
        WritableImage wi = new WritableImage(WIDTH, HEIGHT);
        view.snapshot(new SnapshotParameters(),wi);
        return wi;
    }

    public boolean isVideoPlaying() {
        return isVideoPlaying;
    }
}

